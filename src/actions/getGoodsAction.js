import {getAllGoods} from '../resources/goodsResource';

export const getGoods = () => (dispatch) => {
    getAllGoods()
    .then(response => response.json())
    .then(result => {
        dispatch({
            type: 'GET_GOODS',
            goods: result
        });
    })
};