import {getAllOrders} from '../resources/ordersResource';

export const getOrders = () => (dispatch) => {
    getAllOrders()
    .then(response => response.json())
    .then(result => {
        dispatch({
            type: 'GET_ORDERS',
            orders: result
        });
    })
};