import React from 'react';
import { MdAddCircleOutline } from "react-icons/md";


const imgUrl = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1565102627477&di=53caa14a65cf5b79d1be87b12fdefb17&imgtype=0&src=http%3A%2F%2Fimage2.suning.cn%2Fuimg%2Fb2c%2Fnewcatentries%2F0000000000-000000000124369419_1_800x800.jpg';

const Good = ({id, name, price, unit, url, addOrder}) => {
    return (
        <div className='good'>
            <img src={imgUrl} alt="good image"/>
            <h3>{name}</h3>
            <div className='desc'>
                <span>{`单价:${price}元/${unit}`}</span>
                <MdAddCircleOutline className='icon' onClick={addOrder.bind(this, id)}></MdAddCircleOutline>
            </div>
        </div>
    )
}

export default Good
