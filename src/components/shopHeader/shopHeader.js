import React from 'react';
import {NavLink} from 'react-router-dom';
import {MdHome, MdShoppingCart, MdAdd} from 'react-icons/md';

const ShopHeader = () => {
    return (
        <header>
            <ul>
                <NavLink exact className='link' activeClassName='active' to='/'><li><MdHome className='icon' />商城</li></NavLink>
                <NavLink className='link' activeClassName='active' to='/orders'><li><MdShoppingCart className='icon' />订单</li></NavLink>
                <NavLink className='link' activeClassName='active' to='/goods/create'><li><MdAdd className='icon' />添加商品</li></NavLink>
            </ul>
        </header>
    )
}

export default ShopHeader
