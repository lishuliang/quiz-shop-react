
const BASE_URL = 'http://localhost:8080/api/goods';

export const getAllGoods = () => {
    return fetch(`${BASE_URL}`, {
        method : 'GET',
        headers : {
            'Content-Type' : `application/json;charset=UTF-8`
        }
    })
}

export const createGood = (good) => {
    return fetch(`${BASE_URL}`, {
        method : 'POST',
        headers : {
            'Content-Type' : `application/json;charset=UTF-8`
        },
        body : JSON.stringify(good)
    })
}