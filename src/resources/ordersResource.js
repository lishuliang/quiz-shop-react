const BASE_URL = 'http://localhost:8080/api/orders';

export const getAllOrders = () => {
    return fetch(`${BASE_URL}`, {
        method : 'GET',
        headers : {
            'Content-Type' : `application/json;charset=UTF-8`
        }
    })
}

export const createOrder = (order) => {
    return fetch(`${BASE_URL}`, {
        method : 'POST',
        headers : {
            'Content-Type' : `application/json;charset=UTF-8`
        },
        body : JSON.stringify(order)
    })
}

export const deleteOrder = (id) => {
    return fetch(`${BASE_URL}/${id}`, {
        method : 'DELETE',
        headers : {
            'Content-Type' : `application/json;charset=UTF-8`
        },
    })
}