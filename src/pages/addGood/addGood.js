import React, { Component } from 'react';
import {createGood} from '../../resources/goodsResource';

export default class AddGood extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            price: '',
            unit: '',
            url: ''
        };
        this.nameOnchange = this.handleOnchange.bind(this, 'name');
        this.priceOnchange = this.handleOnchange.bind(this, 'price');
        this.unitOnchange = this.handleOnchange.bind(this, 'unit');
        this.imgOnchange = this.handleOnchange.bind(this, 'url');
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleOnchange(type, event) {
        this.setState({
            [type] : event.target.value
        })
    }

    handleSubmit() {
        createGood(this.state)
        .then(response => {
            console.log(response)
            if (response.status === 201) {
                alert("添加商品成功！")
            }
        })
    }

    render() {
        return (
            <div className='addGood'>
                <h1>添加商品</h1>
                <label><span>*</span>名称:</label>
                <input type="text" placeholder='名称' onChange={this.nameOnchange}/>
                <label><span>*</span>价格:</label>
                <input type="text" placeholder='价格' onChange={this.priceOnchange}/>
                <label><span>*</span>单位:</label>
                <input type="text" placeholder='单位' onChange={this.unitOnchange}/>
                <label><span>*</span>图片:</label>
                <input type="text" placeholder='URL' onChange={this.imgOnchange}/>
                <button onClick={this.handleSubmit}>提交</button>
            </div>
        )
    }
}
