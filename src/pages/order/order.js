import React, { Component } from 'react';
import { Table, Divider} from 'antd';
const { Column } = Table;
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getOrders} from '../../actions/getOrdersAction';
import {deleteOrder} from '../../resources/ordersResource';


class Order extends Component {

    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);
    }

    componentDidMount() {
        this.props.getOrders();
    }

    handleDelete(id) {
        deleteOrder(id)
        .then(response => {
            if(response.status === 200) {
                console.log("删除订单成功！");
                this.props.getOrders();
                this.props.history.push('/orders');
            }
        })
    }

    render() {
        const {orders} = this.props;
        orders.forEach(order => {
            order['key'] = order.id;
        })
        return (
            <main>
                <Table dataSource={orders} pagination={false} rowClassName={'background-color'} size='default'>
                    <Column title='名字' dataIndex='name' key='name' align='center'></Column>
                    <Column title='单价' dataIndex='price' key='price' align='center'></Column>
                    <Column title='数量' dataIndex='quantity' key='quantity' align='center'></Column>
                    <Column title='单位' dataIndex='unit' key='unit' align='center'></Column>
                    <Column
                        title='操作' 
                        dataIndex='action'
                        key="action"
                        render={(text, order) => (
                            <span>
                                <button onClick={this.handleDelete.bind(this, order.id)}><span>删除</span></button>
                            </span>
                        )}
                    />
                </Table>
            </main>
        )
    }
}


const mapDispatchToProps = dispatch =>  bindActionCreators({
    getOrders
}, dispatch);

const mapStateToProps = state => ({
    orders : state.orders.orders
})

export default connect(mapStateToProps, mapDispatchToProps)(Order)

