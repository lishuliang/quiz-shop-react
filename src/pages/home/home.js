import React, { Component } from 'react'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getGoods} from '../../actions/getGoodsAction';
import Good from '../../components/good/Good';
import {createOrder} from '../../resources/ordersResource'

class Home extends Component {

    constructor(props) {
        super(props);
        this.addOrder = this.addOrder.bind(this);
    }

    componentDidMount() {
        this.props.getGoods();
    }

    addOrder(id) {
        createOrder({goodsId:id,quantity: 1})
        .then(response => {
            if(response.status === 201) {
                console.log('订单创建成功');
            }
        })
    }

    render() {
        const {goods} = this.props;
        const goodsItem = goods.map(good => 
            <li key={`good-${good.name}`}>
                <Good id={good.id} name={good.name} price={good.price} unit={good.unit} url={good.url} addOrder={this.addOrder}></Good>
            </li>
        )

        return (
            <main className='goods'>
                <ul>
                    {goodsItem}
                </ul>
            </main>
        )
    }
}

const mapDispatchToProps = dispatch =>  bindActionCreators({
    getGoods
}, dispatch);

const mapStateToProps = state => ({
    goods : state.goods.goods
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)
