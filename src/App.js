import React, {Component} from 'react';
import './App.less';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch, Redirect} from "react-router";
import ShopHeader from './components/shopHeader/ShopHeader';
import Home from './pages/home/Home';
import AddGood from './pages/addGood/AddGood';
import Order from './pages/order/Order';

class App extends Component{
  render() {
    return (
      <div className='App'>
        <Router>
        <ShopHeader></ShopHeader>          
          <Switch>
            <Route path='/goods/create' component={AddGood}></Route>
            <Route path='/orders' component={Order}></Route>
            <Route path='/' component={Home}></Route>
            <Redirect path="/" to={{pathname:'/'}} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;