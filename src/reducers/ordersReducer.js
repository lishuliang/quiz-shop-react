const initState = {
    orders: []
  };
  
  export default (state = initState, action) => {
    switch (action.type) {
      case 'GET_ORDERS':
        return {
          ...state,
          orders: action.orders
        };
  
      default:
        return state
    }
  };