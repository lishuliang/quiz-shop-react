import {combineReducers} from "redux";
import goods from './goodsReducer';
import orders from './ordersReducer';

const reducers = combineReducers({
    goods,
    orders
});
export default reducers;